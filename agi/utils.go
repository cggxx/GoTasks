package agi

import (
    "os"
    "os/exec"
    "path/filepath"
//    "runtime"
    "io/ioutil"
//    "time"
//    "fmt"
//    "bytes"
//    "errors"
//    "log"
)

const (
    LOG = iota  // 只是要输出
    DEBUG       // 调试信息
    WARN        // 警告
    ERROR       // 错误
    FATAL       // 严重错误
)

func Exist(path string) bool {
    _, err := os.Stat(path)
    return err == nil || os.IsExist(err)
}

func GetRuntimeDir() string {
    file, _ := exec.LookPath(os.Args[0])
    return filepath.Dir(file)
}

func ReadFileByte(path string) ([]byte, error) {
    fi, err := os.Open(path)
    if err != nil {
        //		panic(err)
        return nil, err
    } else {
        defer fi.Close()
        return ioutil.ReadAll(fi)
    }
}

func ReadFileStr(path string) (string, error) {
    raw, err := ReadFileByte(path)
    return string(raw), err
}



//
//func GetTraceLevelName(level int) string {
//    switch level {
//        case LOG :
//        return "LOG"
//        case DEBUG :
//        return "DEBUG"
//        case WARN :
//        return "WARN"
//        case ERROR :
//        return "ERROR"
//        default :
//        return "UNKNOWN"
//    }
//}
//
//var timeFormat string = "2006-01-02 15:04:05"
//var traceFormat string = "[%s] %s %s"
//
//func GetOsFlag() int {
//    switch os := runtime.GOOS; os {
//        case "darwin":
//            return OS_X
//        case "linux":
//            return OS_LINUX
//        case "windows":
//            return OS_WIN
//        default:
//            return OS_OTHERS
//    }
//}
//
//func GetOsEol() string {
//    if GetOsFlag() == OS_WIN {
//        return "\r\n"
//    }
//    return "\n"
//}
//

//
//func Concat(delimiter string, input ...interface{}) string {
//    buffer := bytes.Buffer{}
//    l := len(input)
//    for i := 0; i < l; i++ {
//        str := fmt.Sprint(input[i])
//        buffer.WriteString(str)
//        if i < l - 1 {
//            buffer.WriteString(delimiter)
//        }
//    }
//    return buffer.String()
//}
//
//var logLevel = WARN
//var logDir = ""
//var logFileName = "task_service.log"
//var logBuffer = bytes.Buffer{}
//var logBufferSize = 0
//var logBufferMaxSize = 1024
//
//func SetLogLevel(level int) int {
//    if logLevel != level {
//        logLevel = level
//    }
//    return logLevel
//}
//
//func SetLogDir(path string) bool {
//    if len(path) > 0 && Exist(path) {
//        logDir = path
//        return true
//    }
//    return false
//}
//
//func GetLogDir() string {
//    return logDir
//}
//
//func WriteLogBufferToFile() error {
//    // 无效的目录
//    if len(logDir) == 0 {
//        return errors.New("没有指定日志目录")
//    }
//    fileName := logDir + "/" + logFileName
//    file, err := os.OpenFile(fileName, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0777)
//    defer file.Close()
//    if err == nil {
//        _, err = file.Write(logBuffer.Bytes())
//        if err == nil {
//            logBuffer.Truncate(0)
//            logBufferSize = 0
//        }
//    }
//    return err
//}
//
//func Trace(level int, msg ...interface{}) bool {
//    now := time.Now().Format(timeFormat)
//    newMsg := fmt.Sprintf(traceFormat + GetOsEol(), now, GetTraceLevelName(level), Concat(" ", msg...))
//    if level >= logLevel {
//        logBufferSize += len(newMsg)
//        logBuffer.WriteString(newMsg)
//        if logBufferSize > logBufferMaxSize {
//            WriteLogBufferToFile()
//        }
//    }
//    fmt.Print(newMsg)
//    return true
//}
//
//func Log(msg ...interface{}) bool {
//    return Trace(LOG, msg...)
//}
//
//func Debug(msg ...interface{}) bool {
//    return Trace(DEBUG, msg...)
//}
//
//func Warn(msg ...interface{}) bool {
//    return Trace(WARN, msg...)
//}
//
//func Error(msg ...interface{}) bool {
//    return Trace(ERROR, msg...)
//}
//

//
//func HttpGet(reqUrl string, fn func(string)) {
//
//    request := func(ch chan string) bool {
//        resp, err := http.Get(reqUrl)
//        if err != nil {
//            Warn(reqUrl, "读取失败", err)
//            return false
//        }
//        defer resp.Body.Close()
//        body, err := ioutil.ReadAll(resp.Body)
//        if err != nil {
//            Warn(reqUrl, "请求内容加载失败", err)
//            return false;
//        }
//
//        ch <- string(body)
//        return true
//    }
//
//    ch := make(chan string)
//    go request(ch)
//    resp := <-ch
//    fn(resp)
//}

//func HttpGetSync(url string, ch chan string) {
//    resp, err := http.Get(url)
//    if err != nil {
//        // handle error
//    }
//
//    defer resp.Body.Close()
//    body, err := ioutil.ReadAll(resp.Body)
//    if err != nil {
//        Warn(url, "访问失败！", err)
//    }
//
//    ch <- string(body)
//}
