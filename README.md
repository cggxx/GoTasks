#GoTasks

呃，不用说明了吧，直接go build就可以随意玩了。

已在 Ubuntu 上的Go 1.2.x轻松编译通过。

日志库用了这个[https://github.com/donnie4w/go-logger](https://github.com/donnie4w/go-logger)，不过我修改了一些。

```
GoTasks.exe --help
Usage of GoTasks.exe:
  -cycle=43200: 重新加载任务的周期，单位秒
  -log-dir=".": 日志输出目录
  -log-level=1: 日志输出级别
  -tasks="": 任务列表
  -trace-cycle=600: 输出心跳信息的周期，单位秒
  -show-complete=1: 显示请求完成的信息
```

执行

```
GoTasks -tasks="http://localhost/tasks.json" -cycle=43200
```

给一个tasks.json的清单样例：

```json
{
	"Tasks":[
		{
			"Name":"kissOschina",
			"Url":"http://www.oschina.net/",
			"Cycle":1,
			"PostUrl":""
		},
		{
			"Name":"kissBaidu",
			"Url":"http://www.baidu.com/",
			"Cycle":1,
			"PostUrl":""
		}
	]
}
```

* Name 任务名称
* Url 要取得数据的Url
* Cycle 获取的周期，单位秒
* PostUrl 从Url取得的页面的内容，将他Post到另一个Url上

